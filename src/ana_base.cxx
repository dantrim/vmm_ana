#include "ana_base.hh"

// vmm
using namespace vmm_ana;

// std/stl
#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;

// ROOT

///////////////////////////////////////////////////////////////////////////////
AnaBase::AnaBase() :
    m_dbg(0),
    m_draw_all_plots(false),
    m_input_filename(""),
    m_output_directory("./"),
    m_plot_dir("./"),
    m_data_dir("./"),
    m_input_tfile(0),
    m_input_chain(0)
{
}
///////////////////////////////////////////////////////////////////////////////
void AnaBase::set_file(std::string filename)
{
    if(dbg()) cout << "AnaBase::set_file    " << filename << endl;
    m_input_filename = filename;
}
///////////////////////////////////////////////////////////////////////////////
std::string AnaBase::input_filename()
{
    return m_input_filename;
}
///////////////////////////////////////////////////////////////////////////////
void AnaBase::set_output_suffix(std::string suffix)
{
    m_output_suffix = suffix;
}
///////////////////////////////////////////////////////////////////////////////
std::string AnaBase::suffix()
{
    return m_output_suffix;
}
///////////////////////////////////////////////////////////////////////////////
bool AnaBase::set_output_directory(std::string name)
{
    bool ok = true;

    bool dir_exists = std::ifstream(name).good();
    if(!dir_exists) {
        cout << "AnaBase::set_output_directory    WARNING Output directory '" << name << "' does not exist" << endl;
        cout << "AnaBase::set_output_directory    Storing output in default directory '" << m_output_directory << "'" << endl;

        m_output_directory = "./";
        m_plot_dir = "./";
        m_data_dir = "./";

        ok = false;
    }
    else {
        cout << "AnaBase::set_output_directory    Directory '" << name << "' found" << endl;
    }

    if(ok) {
        // check for directory structure
        stringstream sx;
        sx << "./" << name << "/plots";
        if(suffix()!="") sx << "_" << suffix();
        sx << "/";
        string plot_dir = sx.str();
        bool plot_dir_exists = std::ifstream(sx.str()).good();

        sx.str("");
        sx << "./" << name << "/data";
        if(suffix()!="") sx << "_" << suffix();
        sx << "/";
        string data_dir = sx.str();
        bool data_dir_exists = std::ifstream(sx.str()).good();

        if(!plot_dir_exists || !data_dir_exists) {
            cout << endl;
            cout << "AnaBase::set_output_directory    WARNING Either the plot dir ('" << plot_dir << "') or"
            << " the data dir ('" << data_dir << "') does not exist: plot? " << plot_dir_exists
            << "  data? " << data_dir_exists << endl;
            cout << "AnaBase::set_output_directory    Will put all output in directory './" << name << "/'" << endl;
            cout << endl;
        }
        else {
            cout << endl;
            cout << "AnaBase::set_output_directory    Setting data directory : " << data_dir << endl;
            cout << "AnaBase::set_output_directory    Setting plot directory : " << plot_dir << endl;
            cout << endl;

            m_output_directory = name;
            m_plot_dir = plot_dir;
            m_data_dir = data_dir;
        }
    } // ok

    return true;
}
///////////////////////////////////////////////////////////////////////////////
bool AnaBase::load_chain(std::string chain_name)
{
    bool ok = true;
    TFile* tmp_file = TFile::Open(m_input_filename.c_str());
    if(tmp_file) {
        cout << "AnaBase::load_chain    File '" << m_input_filename << "' opened" << endl;

        m_input_tfile = tmp_file;
        m_input_tfile->ls();

        TTree* chain = (TTree*)tmp_file->Get(chain_name.c_str());
        if(chain) {
            cout << "AnaBase::load_chain    Got chain '" << chain_name << "', with entries: "
                << chain->GetEntries() << endl;
            m_input_chain = static_cast<TChain*>(chain);
            ok = connect_branches(chain_name);
        }
        else {
            delete chain;
            cout << "AnaBase::load_chain    ERROR Unable to get chain '" << chain_name << "' from input file: "
                << m_input_filename << "!" << endl;
            ok = false;
        }
    }
    else {
        delete tmp_file;
        cout << "AnaBase::load_chain    ERROR Unable to open ROOT file '" << m_input_filename << "'" << endl;
        ok = false;
    }
    return ok;
}
///////////////////////////////////////////////////////////////////////////////
bool AnaBase::connect_branches(std::string chain_name)
{
    bool ok = true;
    if(chain_name=="calib")
        ok =  connect_calib_branches();
    else if(chain_name=="vmm")
        ok = connect_vmm_branches();
    else {
        cout << "AnaBase::connect_branches    You have requested to load branches for un-handled tree with name '"
            << chain_name << "'! (expect either 'vmm' or 'calib')" << endl;
        ok = false;
    }

    return ok;
}
///////////////////////////////////////////////////////////////////////////////
bool AnaBase::connect_calib_branches()
{
    bool ok = true;

    m_chips = nullptr;
    m_channels = nullptr;
    m_pdo = nullptr;
    m_tdo = nullptr;
    m_is_neighbor = nullptr;
    m_pass_threshold = nullptr;

    try {
        chain()->SetBranchAddress("chip", &m_chips, &br_chips);
        chain()->SetBranchAddress("channel", &m_channels, &br_channels);
        #warning board id is not stored in calib tree
        chain()->SetBranchAddress("pulserCounts", &m_pulser_DAC, &br_pulser_DAC);
        chain()->SetBranchAddress("dacCounts", &m_threshold_DAC, &br_threshold_DAC);
        chain()->SetBranchAddress("tpSkew", &m_tp_skew, &br_tp_skew);
        chain()->SetBranchAddress("pdo", &m_pdo, &br_pdo);
        chain()->SetBranchAddress("tdo", &m_tdo, &br_tdo);
        chain()->SetBranchAddress("isNeighbor", &m_is_neighbor, &br_is_neighbor);
        chain()->SetBranchAddress("threshold", &m_pass_threshold, &br_pass_threshold);
    }
    catch(std::exception &e) {
        cout << "AnaBase::connect_calib_branches    ERROR connecting calib tree branches: " << e.what() << endl;
        ok = false;
    }
    return ok;
}
///////////////////////////////////////////////////////////////////////////////
bool AnaBase::connect_vmm_branches()
{
    cout << "AnaBase::connect_vmm_branches()    Method not implemented!" << endl;
    return false;
}
///////////////////////////////////////////////////////////////////////////////
void AnaBase::get_events()
{
    cout << "AnaBase::get_events()" << endl;
}
///////////////////////////////////////////////////////////////////////////////
void AnaBase::do_analysis(std::string /*type*/)
{
    cout << "AnaBase::do_analysis()" << endl;
}
///////////////////////////////////////////////////////////////////////////////
void AnaBase::add_event(AnaEvent event)
{
    m_ana_events.push_back(event);
}
///////////////////////////////////////////////////////////////////////////////
void AnaBase::load_board(int board)
{
    if(!board_is_loaded(board)) {
        m_loaded_boards.push_back(board);
        std::sort(m_loaded_boards.begin(), m_loaded_boards.end());
    }
}
///////////////////////////////////////////////////////////////////////////////
void AnaBase::load_chip(int chip)
{
    if(!chip_is_loaded(chip)) {
        m_loaded_chips.push_back(chip);
        std::sort(m_loaded_chips.begin(), m_loaded_chips.end());
    }
}
///////////////////////////////////////////////////////////////////////////////
void AnaBase::load_channel(int channel)
{
    if(!channel_is_loaded(channel)) {
        m_loaded_channels.push_back(channel);
        std::sort(m_loaded_channels.begin(), m_loaded_channels.end());
    }
}
///////////////////////////////////////////////////////////////////////////////
bool AnaBase::board_is_loaded(int board)
{
    if(std::find(m_loaded_boards.begin(), m_loaded_boards.end(), board) != m_loaded_boards.end())
        return true;
    return false;
}
///////////////////////////////////////////////////////////////////////////////
bool AnaBase::chip_is_loaded(int chip)
{
    if(std::find(m_loaded_chips.begin(), m_loaded_chips.end(), chip) != m_loaded_chips.end())
        return true;
    return false;
}
///////////////////////////////////////////////////////////////////////////////
bool AnaBase::channel_is_loaded(int channel)
{
    if(std::find(m_loaded_channels.begin(), m_loaded_channels.end(), channel) != m_loaded_channels.end())
        return true;
    return false;
}
///////////////////////////////////////////////////////////////////////////////
bool AnaBase::get_event(BoardChipChannelTuple tuple, AnaEvent& event)
{
    int board = std::get<0>(tuple);
    int chip = std::get<1>(tuple);
    int channel = std::get<2>(tuple);

    if(!board_is_loaded(board)) {
        cout << "AnaBase::get_event    No events with board #" << board << " are loaded" << endl;
        return false;
    }
    if(!chip_is_loaded(chip)) {
        cout << "AnaBase::get_event    No events with chip #" << chip << " are loaded" << endl;
        return false;
    }
    if(!channel_is_loaded(channel)) {
        cout << "AnaBase::get_event    No events with channel #" << channel << " are loaded" << endl;
        return false;
    }

    bool found = false;
    for(auto ev : m_ana_events) {
        int b = ev.board;
        int c = ev.chip;
        int ch = ev.channel;
        if(b==board && c==chip && ch==channel) {
            event = ev;
            found = true;
            break;
        }
    }
    return found;
}
