#include "vmm_time_res.hh"
#warning vmm_time_res.cxx

// vmm
using namespace vmm_ana;

// std/stl
#include <iostream>
#include <iomanip> // setw, setprecision
#include <fstream>
#include <sstream>
using namespace std;
#include <math.h> // sqrt

// ROOT
#include "TROOT.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TGraphSmooth.h"
#include "TF1.h"
#include "TGraphAsymmErrors.h"
#include "TLine.h"
#include "TLatex.h"
#include "TGraph.h"



///////////////////////////////////////////////////////////////////////////////
TimeRes::TimeRes()
{
    cout << "TimeRes::TimeRes()" << endl;
}
///////////////////////////////////////////////////////////////////////////////
bool TimeRes::load_tdo_calibration(std::string calib_file)
{
    cout << "TimeRes::load_tdo_calibration    Loading TAC calibration from file: "
        << calib_file << endl;

    // clear any loaded results
    m_tdo_calib_map.clear();

    string endswith = ".txt";
    size_t endswith_txt = calib_file.find(endswith);
    if(endswith_txt == std::string::npos) {
        cout << "TimeRes::load_tdo_calibration    TAC calibration file is not a *txt file!" << endl;
        return false;
    }

    // assume file check already happened :)
    string fileline;
    std::ifstream infile(calib_file);
    int line_number = -1;

    string board;
    string chip;
    string channel;
    string tac;
    string tac_error;

    vector<int> loaded_boards;
    vector<int> loaded_chips;
    vector<int> loaded_channels;

    while(std::getline(infile, fileline)) {
        line_number++;
        std::istringstream iss(fileline);

        if(line_number==0) continue; // assume first is header
        if( ! (iss >> board >> chip >> channel >> tac >> tac_error ) ) {
            cout << "TimeRes::load_tdo_calibration    ERROR loading TAC calibration from file: "
                << "unexpected format found on line #" << line_number << " of input file: "
                << calib_file << endl;
            return false;
        }

        try {
            int b = std::stoi(board);
            int c = std::stoi(chip);
            int ch = std::stoi(channel);
            std::string::size_type sz;
            float t = std::stof(tac, &sz);
            float te = std::stof(tac_error, &sz);

            if(std::find(loaded_boards.begin(), loaded_boards.end(), b) != loaded_boards.end())
                loaded_boards.push_back(b);
            if(std::find(loaded_chips.begin(), loaded_chips.end(), c) != loaded_chips.end())
                loaded_chips.push_back(c);
            if(std::find(loaded_channels.begin(), loaded_channels.end(), ch) != loaded_channels.end())
                loaded_channels.push_back(ch);

            BoardChipChannelTuple tuple(b, c, ch);
            TDOCalibPair tacpair = std::make_tuple(t, te);
            m_tdo_calib_map[tuple] = tacpair;

        } // try
        catch(std::exception& e) {
            cout << "TimeRes::load_tdo_calibration    ERROR Loading TDO calibration from file '"
                << calib_file << "'' (line #" << line_number << ") : " << e.what() << endl;
            return false;
        } // catch
    } // while

   
    if(dbg()) {
        print_loaded_calibration();

    if(m_tdo_calib_map.size())
        get_global_spread();
 }

    return true;
}
///////////////////////////////////////////////////////////////////////////////
void TimeRes::print_loaded_calibration()
{
    if(!(m_tdo_calib_map.size())) return;

    int wt = 18;
    cout << "TimeRes::print_loaded_calibration" << endl;
    cout << "------------------------------------------------------------------------------------------" << endl;
    cout << std::setw(wt) << "BOARD" << std::setw(wt) << "CHIP" << std::setw(wt) << "CHANNEL"
            << std::setw(wt) << "TAC[cts/ns]" << std::setw(wt) << "ERROR[cts/ns]" << endl;
    cout << "------------------------------------------------------------------------------------------" << endl;
    stringstream sx;
    for(auto map : m_tdo_calib_map) {
        sx.str("");
        BoardChipChannelTuple bcc = map.first;
        TDOCalibPair cal = map.second;

        int board = std::get<0>(bcc);
        int chip = std::get<1>(bcc);
        int channel = std::get<2>(bcc);
        float tac = std::abs(std::get<0>(cal));
        float tac_error = std::get<1>(cal);
        sx << std::setw(wt) << board
            << std::setw(wt) << chip
            << std::setw(wt) << channel
            << std::setw(wt) << std::setprecision(4) << tac
            << std::setw(wt) << std::setprecision(4) << tac_error;
        cout << sx.str() << endl;
    } // map
    cout << "------------------------------------------------------------------------------------------" << endl;
}
///////////////////////////////////////////////////////////////////////////////
void TimeRes::get_global_spread()
{
    std::map<BoardChipTuple, std::vector<float> > board_chip_data;
    for(auto map : m_tdo_calib_map) {
        BoardChipChannelTuple bcc = map.first;
        TDOCalibPair cal = map.second;

        int board = std::get<0>(bcc);
        int chip = std::get<1>(bcc);

        float tac = std::get<0>(cal);

        BoardChipTuple bc(board,chip);

        board_chip_data[bc].push_back(tac);
    }

    stringstream sx;
    for(auto bcdata : board_chip_data) {
        sx.str("");

        BoardChipTuple bc = bcdata.first;
        int board = std::get<0>(bc);
        int chip = std::get<1>(bc);
        vector<float> tacs = bcdata.second;

        sx << "h_global_spread_b"<<board<<"_c"<<chip;
        TH1F h(sx.str().c_str(),"",30,0,30);
        for(auto val : tacs) {
            h.Fill(std::abs(val));
        }

        float mean = h.GetMean();
        float spread = h.GetRMS();

        TDOCalibPair tcp(mean, spread);
        m_chip_global_calib_map[bc] = tcp;
    }

    if(dbg())
        print_global_calib_map();
}
///////////////////////////////////////////////////////////////////////////////
void TimeRes::print_global_calib_map()
{
    if(!(m_chip_global_calib_map.size())) return;

    int wt = 18;
    cout << "TimeRes::print_global_calib_map" << endl;
    cout << "----------------------------------------------------------------------------" << endl;
    cout << std::setw(wt) << "BOARD" << std::setw(wt) << "CHIP" << std::setw(wt) << "MEANTAC[cts/ns]"
            << std::setw(wt) << "SPREAD[cts/ns]" << endl;
    cout << "----------------------------------------------------------------------------" << endl;
    stringstream sx;
    for(auto map : m_chip_global_calib_map) {
        sx.str("");
        BoardChipTuple bct = map.first;
        TDOCalibPair cal = map.second;

        int board = std::get<0>(bct);
        int chip = std::get<1>(bct);
        float tac = std::get<0>(cal);
        float err = std::get<1>(cal);

        sx << std::setw(wt) << board << std::setw(wt) << chip
                << std::setw(wt) << tac << std::setw(wt) << err;
        cout << sx.str() << endl;
    } // map
    cout << "----------------------------------------------------------------------------" << endl;
}
///////////////////////////////////////////////////////////////////////////////
void TimeRes::do_analysis(std::string type)
{
    cout << "TimeRes::do_analysis    *** Starting analysis : " << type << " *** " << endl;

    // load up events
    get_events();

    if(type=="res-charge") {
        do_time_resolution_vs_charge();
    }

}
///////////////////////////////////////////////////////////////////////////////
void TimeRes::do_time_resolution_vs_charge()
{
    cout << "TimeRes::do_time_resolution_vs_charge()" << endl;

    group_events_by_charge();

    for(auto dac : m_loaded_pulser_dacs) {
        get_res_for_dac(dac);
    } 

    make_resolution_vs_charge_plots();

}
///////////////////////////////////////////////////////////////////////////////
void TimeRes::group_events_by_charge()
{
    for(auto event : events()) {
        int dac = event.pulser_DAC;
    //}
    //for(auto b : loaded_boards()) {
    //    for(auto c : loaded_chips()) {
    //        for(auto ch : loaded_channels()) {
    //            BoardChipChannelTuple t(b,c,ch);
    //            AnaEvent event;
    //            if(!get_event(t,event)) continue;

    //            int dac = event.pulser_DAC;
                if(std::find(m_loaded_pulser_dacs.begin(), m_loaded_pulser_dacs.end(), dac)
                                        == m_loaded_pulser_dacs.end())
                    m_loaded_pulser_dacs.push_back(dac);
     //       } // c
     //   } // c
    } // b

    cout << "loaded pulser dacs: ";
    for(auto dac : m_loaded_pulser_dacs) cout <<  " " << dac;
    cout << endl;

    for(auto dac : m_loaded_pulser_dacs) {
        int n_for_dac = 0;
        for(auto b : loaded_boards()) {
            for(auto c : loaded_chips()) {
                for(auto ch : loaded_channels()) {
                    BoardChipChannelTuple t(b,c,ch);
                    for(auto& event : events()) {

                        if(event.pulser_DAC != dac) continue;

                        m_events_by_charge[dac].push_back(event);
                        n_for_dac++;
                    } // event
                } // ch
            } // c
        } // b
        cout << "TimeRes::group_events_by_charge    Grouped " << n_for_dac << " events for Pulser DAC " << dac << endl;
    } // dac

}
///////////////////////////////////////////////////////////////////////////////
void TimeRes::get_res_for_dac(int pulser)
{
    cout << "TimeRes::get_res_for_dac    Pulser DAC " << pulser << endl;

    stringstream sx;
    stringstream name;

    int n_plots_drawn = 0;
        int n_warnings = 0;


    gStyle->SetOptStat(0);
    gStyle->SetOptFit(111);

    for(auto b : loaded_boards()) {
        for(auto c : loaded_chips()) {
            for(auto ch : loaded_channels()) {

                // get the calibration for this channel
                #warning assuming tdo calibration independent of pulser DAC
                BoardChipChannelTuple t(b,c,ch);
                TDOCalibPair cal = m_tdo_calib_map[t];

                vector<AnaEvent> events;
                for(auto event : m_events_by_charge[pulser]) {
                    if(event.board==b && event.chip==c && event.channel==ch) events.push_back(event); 
                } // event

                // now we have events for this bct and pulser DAC

                vector<float> times;
                int n_bins = 0;
                for(auto ev : events) {
                    uint32_t in_tdo = ev.tdo;
                    #warning NEED CHECK IN CASE WE LOAD FILE THAT DOES NOT HAVE GIVEN BOARD,CHIP,CHANNEL ETC
                    float inv_conversion = std::abs(std::get<0>(cal)); // counts/ns
                    #warning setting TDO conversion by hand not from file
                    if(n_warnings<2) {
                        cout << "TimeRes::get_res_for_dac     WARNING HARD CODING TDO CONVERSION TO 2.97" << endl;
                        n_warnings++;
                    }
                    inv_conversion = 2.97; 
                    float conversion = 1.0 / inv_conversion; 
                    n_bins = 256 * conversion;
                    float time = in_tdo * conversion;
                    times.push_back(time);
                }
                cout << "n times: " << times.size() << endl;

                std::sort(times.begin(), times.end());

                float min_time = times.at(0);
                float max_time = times.at(times.size()-1);
                float min_x = 0.95*min_time;
                float max_x = 1.05*max_time;

                sx << "res_for_dac" << pulser << "_b"<<b<<"_c"<<c<<"_ch"<<ch;
                name << "h_" << sx.str();
                TH1F h(name.str().c_str(), "", n_bins, min_x, max_x); 
                h.Sumw2();

                h.GetXaxis()->SetTitle("Time [ns]");
                h.GetXaxis()->SetTitleFont(42);
                h.GetXaxis()->SetLabelFont(42);
                h.GetYaxis()->SetTitle("Entries");
                h.GetYaxis()->SetTitleFont(42);
                h.GetYaxis()->SetLabelFont(42);

                h.SetLineColor(kBlack);

                int max_y = -1;
                for(auto et : times) {
                    h.Fill(et);
                }
                max_y = h.GetMaximum();
                max_y = 1.15*max_y;

                name.str("");
                name << "c_" << sx.str();
                TCanvas* can = new TCanvas(name.str().c_str(), "", 800,600);
                can->SetRightMargin(0.3*can->GetRightMargin());
                can->SetGrid(1,1);
                can->cd();

                h.Draw("hist e");
                can->Update();

                // fit a gaussian 
                name.str("");
                name << "f_" << sx.str();
                TF1* f = new TF1(name.str().c_str(), "gaus", min_time, max_time);
                f->SetParNames("Constant", "Mean", "Sigma");
                f->SetLineColor(kRed);
                f->SetLineWidth(2);
                f->SetLineStyle(1);
                h.Fit(f,"QR");
                f->Draw("same");

                float mean = f->GetParameter(1);
                float sigma = f->GetParameter(2);
                float sigma_error = f->GetParError(2);
                //#warning not using gaussian fit values
                //float mean = h.GetMean();
                //float sigma = h.GetRMS();

                // store these results for later
                //std::tuple<BoardChipChannelTuple, float, float> res_tuple = std::make_tuple(t, mean, sigma);
                //m_dac_mean_res_map[pulser] = res_tuple;

                TDOResPair tcp(mean, sigma, sigma_error);
                //std::tuple<int, TDOCalibPair> ttcp(pulser, tcp);
                m_dac_mean_res_map[t][pulser] = tcp;

                if(all_plots() || n_plots_drawn < 10) {
                    name.str("");
                    name << plot_dir();
                    name << "res_charge_time_samples_dac" << pulser << "_b" << b << "_c" << c << "_ch" << ch;
                    if(suffix()!="") name << "_" << suffix();
                    name << ".eps";
                    can->SaveAs(name.str().c_str());
                    n_plots_drawn++;
                }
                delete can;

                //#warning forcing only one plot to be made
                //return;

            } // ch
        } // c
    } // b

}
///////////////////////////////////////////////////////////////////////////////
void TimeRes::make_resolution_vs_charge_plots()
{
    cout << "TimeRes::make_resolution_vs_charge_plots()" << endl;

    std::sort(m_loaded_pulser_dacs.begin(), m_loaded_pulser_dacs.end());

    stringstream sx;
    stringstream name;

    int n_plots_drawn = 0;

    for(auto b : loaded_boards()) {
        for(auto c : loaded_chips()) {

            int n_total = 0;
            int n_x2 = 0;
            int n_x = 0;

            for(auto ch : loaded_channels()) {
                sx.str("");
                name.str("");

                BoardChipChannelTuple tuple(b, c, ch);


                sx << "res_vs_q_b"<<b<<"_c"<<c<<"_ch"<<ch; 
                name << "c_" << sx.str();
                TCanvas* can = new TCanvas(name.str().c_str(), "", 1200, 600);
                can->SetGrid(1,1);
                can->SetRightMargin(0.4*can->GetRightMargin());
                can->cd();

                int n_bins = m_loaded_pulser_dacs.size(); // one extra under and one extra upper
                int min_dac = m_loaded_pulser_dacs.at(0);
                int max_dac = m_loaded_pulser_dacs.at(m_loaded_pulser_dacs.size()-1);

                min_dac = (min_dac - 20);
                max_dac = (max_dac + 20);

                #warning HARDCODING PDO CALIBRATION
                float charge = 1.0;
                min_dac = 300 * (min_dac * 0.00108); // 300 fF capacitory, 1.08 mV/counts
                max_dac = 300 * (max_dac * 0.00108); // 300 fF capacitory, 1.08 mV/counts

                name.str("");
                name << "axis_" << sx.str();
                TH1F axis(name.str().c_str(), "", n_bins, min_dac, max_dac);
                #warning need to convert pulser DAC to fC!!
                axis.GetXaxis()->SetTitle("Injected Charge [fC]");
                //axis.GetXaxis()->SetTitle("Pulser DAC [cts]");
                axis.GetXaxis()->SetTitleFont(42);
                axis.GetXaxis()->SetLabelFont(42);
                axis.GetYaxis()->SetTitle("Time [ns]");
                axis.GetYaxis()->SetTitleFont(42);
                axis.GetYaxis()->SetLabelFont(42);

                std::map<int, TDOResPair> mean_res_map = m_dac_mean_res_map[tuple];

                TGraphAsymmErrors g(m_loaded_pulser_dacs.size()); 
                g.SetMarkerStyle(20);
                g.SetMarkerSize(1.5*g.GetMarkerSize());
                g.SetMarkerColor(kBlack);

                float max_y = -1;
                float min_y = 1000;

                vector<float> variance_points;
                vector<float> variance_points2;
                vector<float> variance_errors;
                TH1F hvar("hvar","",300,0,100);

                for(int idac = 0; idac < (int)m_loaded_pulser_dacs.size(); idac++) {
                    int dac = m_loaded_pulser_dacs.at(idac);
                    //int check_dac = mean_res_map.first;
                    int check_dac = dac;
                    TDOResPair tcp = mean_res_map[dac];
                    //int check_dac = std::get<0>(mean_res_pair);
                    //TDOCalibPair tcp = std::get<1>(mean_res_pair);

                    if(check_dac != dac) continue;


                    float mean = std::get<0>(tcp);
                    float res  = std::get<1>(tcp);
                    float res_error = std::get<2>(tcp);

                    //cout << "summary for dac: " << dac << "  " << mean << endl;
                    if( (mean+res) > max_y) max_y = (mean+res);
                    if( (mean-res) < min_y) min_y = (mean-res);

                    //g.SetPoint(idac, dac, 35);

                    #warning HARDCODING PDO CALIBRATION
                    float charge = 1.0;
                    charge = 300 * (dac * 0.00108); // 300 fF capacitory, 1.08 mV/counts


                    g.SetPoint(idac, charge, mean);
                    //g.SetPoint(idac, dac, mean);
                    g.SetPointError(idac, 0.0, 0.0, 0.0, 0.0);

                    if(n_total>0) {
                        float varc = hvar.GetRMS();
                        variance_points2.push_back(res);
                        variance_errors.push_back(res_error);
                    }
                    n_x += mean;
                    n_x2 += mean*mean;
                    n_total++;
                    hvar.Fill(mean);

                    n_x2 = (n_x2/n_total);
                    n_x = (n_x/n_total);
                    variance_points.push_back(std::sqrt((n_x2 - n_x)));
                }

                float initial_var = variance_points2.at(1);
                vector<float> variances;
                vector<int> variances_x;
                float last_var = initial_var;
                for(int i = 1; i < (int)variance_points2.size(); i++) {
                    
                    float v = variance_points2.at(i);
                    cout << "WARNING FIXING RES TO BE BETWEEN 0.9 && 0.3!!" << endl;
                    if(v>0.9 || v<0.3) 
                        continue;
                    variances_x.push_back(i);
                    
                    variances.push_back(v);
                }

                TGraphAsymmErrors gv(variances_x.size());
                gv.SetLineColor(kBlack);
                //gv.SetLineColor(12);
                gv.SetLineWidth(2); 
                gv.SetLineStyle(1);


                float min_v = 99999;
                float max_v = -1;
                for(int iv = 0; iv < (int)variances_x.size(); iv++) {
                    int idx = variances_x.at(iv);
                    float err = variance_errors.at(idx);
                    float x = m_loaded_pulser_dacs.at(idx) * 300 * 0.00108;
                    gv.SetPoint(iv, x, variances.at(iv));
                    gv.SetPointError(iv,0.0,0.0,0.0,0.0);

                    if(variances.at(iv)>max_v) max_v = variances.at(iv);
                    if(variances.at(iv)<min_v) min_v = variances.at(iv);
                } // iv

                

                max_y = 1.03*max_y;
                min_y = 0.97*min_y;
                axis.SetMaximum(max_y);
                axis.SetMinimum(min_y);

                can->cd();
                axis.Draw("axis");
                can->Update();
                g.Draw("p same");
                can->Update();

                if(all_plots() || n_plots_drawn < 10) {
                    name.str("");
                    name << plot_dir();
                    name << "res_charge_times_vs_q_summary_b"<<b<<"_c"<<c<<"_ch"<<ch;
                    if(suffix()!="") name << "_" << suffix();
                    name << ".eps";
                    can->SaveAs(name.str().c_str());
                    n_plots_drawn++;
                }
                delete can;

                name.str("");
                name << "cgv_" << sx.str();
                can = new TCanvas(name.str().c_str(),"",1200,600);
                can->SetGrid(1,1);
                can->cd();

                name.str("");
                name << "axis2_" << sx.str();
                TH1F axis2(name.str().c_str(), "", n_bins, min_dac, max_dac);
                #warning need to convert pulser DAC to fC!!
                axis2.GetXaxis()->SetTitle("Injected Charge [fC]");
                //axis2.GetXaxis()->SetTitle("Pulser DAC [cts]");
                axis2.GetXaxis()->SetTitleFont(42);
                axis2.GetXaxis()->SetLabelFont(42);
                axis2.GetXaxis()->SetTitleSize(1.2*axis2.GetYaxis()->GetTitleSize());
                axis2.GetYaxis()->SetTitle("#sigma_{time} [ns]");
                axis2.GetYaxis()->SetTitleFont(42);
                axis2.GetYaxis()->SetLabelFont(42);
                axis2.GetYaxis()->SetTitleSize(1.3*axis2.GetYaxis()->GetTitleSize());
                
                max_v = 1.08*max_v;
                min_v = 0.92*min_v;
                axis2.SetMaximum(max_v);
                axis2.SetMinimum(min_v);

                axis2.Draw("axis");
                can->Update();
                gv.Draw("l same");
                can->Update();
                can->Update();
                gStyle->SetOptFit(0);
                TF1* ff = new TF1("ff","pol2",0, 300);
                ff->SetLineColor(46);
                gv.Fit(ff,"QB");
                ff->Draw("l same");
                can->Update();

                name.str("");
                name << plot_dir();
                name << "res_charge_res_vs_q_summary_b"<<b<<"_c"<<c<<"_ch"<<ch;
                if(suffix()!="") name << "_" << suffix();
                name << ".eps";
                can->SaveAs(name.str().c_str());
                delete can;

            } // ch


        } // c
    } //b


}
///////////////////////////////////////////////////////////////////////////////
void TimeRes::get_events()
{
    cout << "TimeRes::get_events()" << endl;
    stringstream sx;

    bool ok = true;
    Long64_t nentries = chain()->GetEntries();
    Long64_t current_entry = 0;
    Long64_t nb = 0;
    int update_level = 1000;
    update_level = nentries / 20;
    Long64_t for_update = 0;
    int n_checks = 0;

    for(Long64_t jentry = 0; jentry < nentries; jentry++) {
        nb = chain()->GetEntry(jentry);
        if(jentry % update_level == 0 || jentry==(nentries-1)) {
            for_update = (jentry + 1);
            cout << "TimeRes    *** Processing entry : " << std::setw(14) << for_update << "/" << nentries
                        << "  [" << std::setw(3) << n_checks*5 << "\%] *** " << endl;
            n_checks++;
        } // update


        /// build up the hits for this event
        current_entry = jentry + 1;
        size_t number_of_chips = m_tdo->size();
        for(size_t ichip = 0; ichip < number_of_chips; ichip++) {
            uint32_t chipno = m_chips->at(ichip);
            #warning hardcoding board id to 1
            uint32_t boardno = 1;

            vector<int> _tdo = m_tdo->at(ichip);
            vector<int> _pdo = m_pdo->at(ichip); 
            vector<int> _channel = m_channels->at(ichip);
            vector<int> _passes_threshold = m_pass_threshold->at(ichip);

            size_t number_of_hits = _pdo.size();
            for(size_t ihit = 0; ihit < number_of_hits; ihit++) {

                if(_passes_threshold.at(ihit)==0) continue;
                if(_tdo.at(ihit)==0) continue;

                AnaEvent event;

                // coordinates
                event.board = boardno;
                event.chip = chipno;
                event.channel = _channel.at(ihit); 

                // datum
                event.pulser_DAC = m_pulser_DAC;
                event.threshold_DAC = m_threshold_DAC;
                event.tp_skew = m_tp_skew;
                #warning need to store gain in calib ntupleew;
                //event.gain = m_gain; 
                event.pdo = _pdo.at(ihit);
                event.tdo = _tdo.at(ihit);


                #warning hardcoding channel 8 requirement
                //if(event.tp_skew==10) {
                if(event.channel == 8) {
                    add_event(event);

                    load_board(event.board);
                    load_chip(event.chip);
                    load_channel(event.channel);
                }
                
            } // ihit
        } // ichip
    } // jentry

    cout << "TimeRes::get_events    Total events loaded: " << n_events() << endl;
    cout << "loaded boards: ";
    for(auto b : loaded_boards()) cout << " " << b;
    cout << endl;
    cout << "loaded chips: ";
    for(auto c : loaded_chips()) cout << " " << c;
    cout << endl;
    cout << "loaded channels: " ;
    for(auto ch : loaded_channels()) cout << " " << ch;
    cout << endl;

    int board = 1;
    int chip = 0;
    int channel = 24;
    BoardChipChannelTuple t(board, chip, channel);
    AnaEvent event;
    if(get_event(t, event)) {
        cout << "event board : " << event.board << " ch: " << event.channel << " pdo: " << event.pdo << endl;
    }
}

///////////////////////////////////////////////////////////////////////////////
