
// std/stl
#include <iostream>
#include <fstream>
using namespace std;

// vmm
#include "vmm_time_res.hh"
//#include "ana_base.hh"


// ROOT
#include "TFile.h"


void help()
{
    cout << "--------------------------------------------" << endl;
    cout << " * vmm timing analysis *                    " << endl;
    cout << " Usage: vmm_ana_time -i <input-root-file> [other arguments]" << endl;
    cout << endl;
    cout << " Options:" << endl;
    cout << "  -i|--input                   input VMM data ntuple" << endl;
    cout << "  -o|--output                  directory to place output (plots, data, etc...) (default: ./)" << endl;
    cout << "  --name                       set name to append to output directory, data, plots, etc... (default: "")" << endl;
    cout << " --tdo-calib-file              *.txt file containing the TDO calibration (default: "")" << endl;
    cout << " -t|--type                     time resolution analysis type (res-charge, res-skew) (default: res-charge)" << endl;
    cout << "  --all-plots                  make all plots (by default, only stores a subset)" << endl;
    cout << "  -d|--dbg                     set the debug level (default: 0)" << endl; 
    cout << "  -h|--help                    print this help message" << endl;
    cout << "--------------------------------------------" << endl;
    return;
}

int main(int argc, char* argv[])
{

    string input_file = "";
    string output_directory = "./";
    string suffix = "";
    string tdo_calib_file = "";
    bool store_all_plots = false;
    string type = "res-charge";
    int dbg = 0;

    int optin(1);
    while(optin < argc) {
        string in = argv[optin];
        if      (in == "-i" || in == "--input"  ) { input_file = argv[++optin]; }
        else if (in == "-o" || in == "--output" ) { output_directory = argv[++optin]; }
        else if (in == "--name")                  { suffix = argv[++optin]; }
        else if (in == "--tdo-calib-file")        { tdo_calib_file = argv[++optin]; }
        else if (in == "-d" || in == "--dbg")     { dbg = atoi(argv[++optin]); }
        else if (in == "-t" || in == "--type")    { type = argv[++optin]; }
        else if (in == "--all-plots")             { store_all_plots = true; }
        else {
            cout << "Unknown command line argument '" << in << "'" << endl;
            help();
            return 1;
        }
        optin++;
    } // while

    // check
    if(!(type=="res-charge" || type=="res-skew")) {
        cout << "ERROR Invalid input for analysis type provided: " << type << endl;
        cout << "ERROR Only accept 'res-charge' or 'res-skew'" << endl;
        return 1;
    }


    /////////////////////////////////////////////////////////////////
    // get the input file(s)
    /////////////////////////////////////////////////////////////////
    bool file_found = std::ifstream(input_file.c_str()).good();
    if(!file_found) {
        cout << "ERROR Bad input file '" << input_file << "' --> Exitting!" << endl;
        return 1;
    }

    bool calib_file_found = std::ifstream(tdo_calib_file).good();
    if(!calib_file_found) {
        cout << "ERROR Bad input calib file '" << tdo_calib_file << "' --> Exitting!" << endl;
        return 1;
    }

    /////////////////////////////////////////////////////////////////
    // setup the analysis looper
    /////////////////////////////////////////////////////////////////
    vmm_ana::TimeRes* ana = new vmm_ana::TimeRes();
    ana->set_debug(dbg);
    ana->set_file(input_file);
    string chain_name = "calib";
    ana->load_chain(chain_name);
    if(!ana->load_tdo_calibration(tdo_calib_file)) {
        cout << "ERROR Problem loading TDO calibration file '" << tdo_calib_file << "' : "
            << "Is the format correct?" << endl;
        return 1;
    }

    ana->set_output_directory(output_directory);
    ana->set_output_suffix(suffix);

    /////////////////////////////////////////////////////////////////
    // start the analysis
    /////////////////////////////////////////////////////////////////
    ana->do_analysis(type);



    delete ana;

    return 0;
}

