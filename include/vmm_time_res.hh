#ifndef VMM_TIME_RES_HH
#define VMM_TIME_RES_HH

#warning time_rest_hh

///////////////////////////////////////////////////////////////////////////////
//
// vmm_time_res
//
// looper for time resolution studies
//
// daniel.joseph.antrim@cern.ch
// March 2017
//
///////////////////////////////////////////////////////////////////////////////

// vmm
#include "ana_base.hh"
#include "ana_types.hh" // BoardChipChannelTuple, BoardChipTuple
#include "ana_event.hh"

typedef std::tuple<float, float> TDOCalibPair; // TDO conversion, TDO conversion error
typedef std::tuple<float, float, float> TDOResPair; // mean, res, res-error


// ROOT

// 

namespace vmm_ana {

    class TimeRes : public AnaBase
    {
        public :
            TimeRes();
            virtual ~TimeRes(){};


            void do_analysis(std::string ana_type); // res-charge, res-skew
            void do_time_resolution_vs_charge();

            // load up the events from the tree
            void get_events();
            void group_events_by_charge();
            void get_res_for_dac(int pulser_dac);
            void make_resolution_vs_charge_plots();

            // load the tdo calibration constants
            bool load_tdo_calibration(std::string calib_file);
            void get_global_spread();
            std::map<BoardChipChannelTuple, TDOCalibPair> calib_map() { return m_tdo_calib_map; }

            void print_loaded_calibration();
            void print_global_calib_map();

        private :

            std::map<BoardChipChannelTuple, TDOCalibPair> m_tdo_calib_map; 
            std::map<BoardChipTuple, TDOCalibPair> m_chip_global_calib_map;

            // res per charge holders
            std::map<int, std::vector<AnaEvent> > m_events_by_charge; // [ PDO : events ] 
            std::vector<int> m_loaded_pulser_dacs;
            std::map<BoardChipChannelTuple, std::map<int, TDOResPair> > m_dac_mean_res_map;

    }; // class TimeRes


}; // namespace vmm_ana

#endif
