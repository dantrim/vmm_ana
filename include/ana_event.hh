#ifndef ANA_EVENT_HH
#define ANA_EVENT_HH

// std/stl
#include <cstdlib>

namespace vmm_ana {

    class AnaEvent
    {

        public :
            AnaEvent();
            virtual ~AnaEvent(){};

            // calib tree
            uint32_t board;
            uint32_t chip;
            uint32_t channel;

            uint32_t pulser_DAC;
            uint32_t threshold_DAC;
            double tp_skew;
            double gain;
            uint32_t pdo;
            uint32_t tdo;
            bool is_neighbor;
            bool passes_threshold;

        private :

    }; // AnaEvent

}; // vmm_ana


#endif
