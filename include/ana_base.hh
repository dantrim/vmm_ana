#ifndef VMM_ANA_BASE_HH
#define VMM_ANA_BASE_HH

///////////////////////////////////////////////////////////////////////////////
//
// vmm_ana_base
//
// base for all analysis loopers
//
// daniel.joseph.antrim@cern.ch
// March 2017
//
///////////////////////////////////////////////////////////////////////////////

// vmm
#include "ana_event.hh"
#include "ana_types.hh"

// ROOT
#include "TROOT.h"
#include "TStyle.h"
#include "TChain.h"
#include "TFile.h"


namespace vmm_ana {

    class AnaBase
    {
        public :
            AnaBase();
            virtual ~AnaBase(){};

            virtual void set_debug(int dbg) { m_dbg = dbg; }
            virtual int dbg() { return m_dbg; }

            void set_all_plots() { m_draw_all_plots = true; }
            bool all_plots() { return m_draw_all_plots; }

            // input file stuff
            virtual void set_file(std::string filename);
            virtual std::string input_filename();

            TChain* chain() { return m_input_chain; }

            // load input tchain
            bool load_chain(std::string chain_name);
            bool connect_branches(std::string chain_name);
            bool connect_calib_branches();
            bool connect_vmm_branches();

            // output directories for the stuff
            virtual bool set_output_directory(std::string name);
            virtual void set_output_suffix(std::string suffix);
            virtual std::string suffix();
            std::string plot_dir() { return m_plot_dir; }
            std::string data_dir() { return m_data_dir; }


            // load the data
            virtual void do_analysis(std::string ana_type);
            virtual void get_events();
            virtual void add_event(AnaEvent event);
            AnaEvent& event(BoardChipChannelTuple tuple);
            bool get_event(BoardChipChannelTuple tuple, AnaEvent& event);
            std::vector<AnaEvent> events() { return m_ana_events; }
            virtual double n_events() { return m_ana_events.size(); }

            void load_board(int board);
            void load_chip(int chip);
            void load_channel(int channel);

            std::vector<int> loaded_boards() { return m_loaded_boards; }
            std::vector<int> loaded_chips() { return m_loaded_chips; }
            std::vector<int> loaded_channels() { return m_loaded_channels; }

            bool board_is_loaded(int board);
            bool chip_is_loaded(int chip);
            bool channel_is_loaded(int channel);


            // make data public -- need to be smarter about accessing from derived class
            // calib tree data
            uint32_t m_board;
            int32_t m_pulser_DAC;
            int32_t m_threshold_DAC;
            double m_tp_skew;
            std::vector<int>* m_chips;
            std::vector< std::vector<int> >* m_channels;
            std::vector< std::vector<int> >* m_pdo;
            std::vector< std::vector<int> >* m_tdo;
            std::vector< std::vector<int> >* m_is_neighbor;
            std::vector< std::vector<int> >* m_pass_threshold;

        private :
            int m_dbg; // debug level
            bool m_draw_all_plots;

            std::vector<AnaEvent> m_ana_events;

            std::string m_input_filename;

            std::string m_output_directory;
            std::string m_plot_dir;
            std::string m_data_dir;
            std::string m_output_suffix;

            std::vector<int> m_loaded_boards;
            std::vector<int> m_loaded_chips;
            std::vector<int> m_loaded_channels;

            // ROOT objects
            TFile* m_input_tfile;
            TChain* m_input_chain;

            // calib tree
            TBranch *br_board;
            TBranch *br_chips;
            TBranch *br_channels;
            TBranch *br_neighbor_enabled;
            TBranch *br_pulser_DAC;
            TBranch *br_threshold_DAC;
            TBranch *br_pdo;
            TBranch *br_tdo;
            TBranch *br_pass_threshold;
            TBranch *br_is_neighbor;
            TBranch *br_tp_skew;


    }; // class AnaBase


}; // namespace vmm_ana

#endif
